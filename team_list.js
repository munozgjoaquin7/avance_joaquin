
let cur_dialog_temp = {}
//var globalOnload = frappe.listview_settings['Requerimiento'].onload;

frappe.listview_settings['Team'] = {
    refresh: function(frm) {

		
		
		frappe.realtime.on("add_to_team_progress", function(data) {
			if(data.progress) {
				frappe.hide_msgprint(true);

				frappe.show_progress(__("Asignando especialistas "), data.progress[0],data.progress[1],data.detail);
			}
		})

		

	},
    onload: function (doclist) {

    
        const action = () => {
            
            frappe.listview_settings["Team"].add_team_to_req();
            
        };
        const action2 = () => {
            
            frappe.listview_settings["Team"]. export_team_joaquin();
            
        };
        doclist.page.add_button(__('Asignar a requerimiento'), action, false);
        doclist.page.add_button(__('Exportar especialistas'), action2, false);

    },

    export_team_joaquin:function(listview){
        frappe.confirm(
            __("¿Desea exportar especialistas en formato xlsx ? "),
            () => {
                
                var filters  = cur_list.get_filters_for_args();

                frappe.call({
                    type:"GET",
                    args: {'filters': filters, type:args.tipo_asignacion,requerimiento:args.requerimiento,lista_perfil:selected_attributes },
                    method:"hubteam.hubteam.doctype.requerimiento.requerimiento.exportar_especialistas_preseleccionados",
                    freeze: true,
                }).done((data) => {

                                        

                    
                    //console.log(data);
                    
                   // window.open("/api/method/hubteam.hubteam.doctype.requerimiento.requerimiento.exportar_especialistas_preseleccionados?requerimiento="+frm.doc.name);
                   
                    //frm.reload_doc();
                }).fail(() => {
                    frappe.msgprint(__(""));
                });
            },
            //false
            () => {

            }
        );

    },


    add_team_to_req: function(listview) {
        




		cur_dialog_temp = new frappe.ui.Dialog({
			title: __("Asignar al requerimiento"),
			fields: [
				{	"fieldtype": "Link", "label": __("Requerimiento"),
					"fieldname": "requerimiento",
					"options": "Requerimiento",
					"reqd": 1,
					"default": "",
                    onchange: function(){
                        
                        if ((this.value != "") && (this.value !=  undefined )){
                            frappe.call({
                                type:"GET",
                                args: {'requerimiento':this.value},
                                method:"hubteam.hubteam.doctype.requerimiento.requerimiento.list_profile",
                                freeze: true,
                            }).done((data) => {
                                
                                
                                
                                //cur_dialog_temp.fields_dict.perifl.$wrapper.html('<b>Nuevo dato</b>.');
                                if (data.message.length> 0){
                                    
                                    //Agrupar por perfil
                                    /*
                                    const result = Object.entries(data.message.reduce((acc, { name, moneda_presupuesto, perfil_solicitado }) => {
                                        acc[perfil_solicitado] = (acc[perfil_solicitado] || []);
                                        acc[perfil_solicitado].push({ name, moneda_presupuesto });
                                        return acc;
                                      }, {})).map(([key, value]) => ({ name: key, children: value }));

                                    console.log(result);
                                    var fields_profile = []
                                    */
                                   
                                    var html = "";
                                    data.message.forEach(i => {
                    
                                        /*if(i % 3 === 0){
                                            fields.push({fieldtype: 'Section Break'});
                                        }*/					
                                        
                                        /*fields_profile.push({
                                            fieldtype: 'Check',
                                            label: i.perfil_solicitado + " ( "+ i.moneda_presupuesto +" - " + i.presupuesto +")",
                                            fieldname: i.perfil_solicitado,
                                            default: 0
                                        });*/
    
                                        /*
                                        html += `
                                        <div class="form-group"  data-fieldname="`+ i.name + `" title="`+ i.perfil_solicitado + " ( "+ i.moneda_presupuesto +" - " + i.presupuesto +")" +`">
                                            <div class="checkbox">
                                                <label>
                                                    <span class="input-area" style="display: inline;">
                                                        <input type="checkbox" autocomplete="off" class="input-with-feedback" data-fieldtype="Check" data-fieldname="`+ i.name + `" placeholder="">
                                                    </span>
                                                   
                                                    <span class="label-area">`+ i.perfil_solicitado + " ( "+ i.moneda_presupuesto +" - " + i.presupuesto +")" +`</span>
                                                </label>
                                            </div>
                                        </div>
                                        `;
                                        */


                                        html += `
                                        <div class="form-group"  data-fieldname="`+ i.name + `" data-fieldvalue="`+ i.perfil_solicitado + `" title="`+ i.perfil_solicitado +`">
                                            <div class="checkbox">
                                                <label>
                                                    <span class="input-area" style="display: inline;">
                                                        <input type="checkbox" autocomplete="off" class="input" data-fieldtype="Check" data-fieldname="`+ i.name + `" data-fieldvalue="`+ i.perfil_solicitado + `" placeholder="">
                                                    </span>
                                                   
                                                    <span class="label-area">`+ i.perfil_solicitado +" (" + i.moneda_presupuesto+ " "+ i.presupuesto +`)</span>
                                                </label>
                                            </div>
                                        </div>
                                        `;
                                        
                                    });
                                       
                                    cur_dialog_temp.fields_dict.html_perfil.$wrapper.html(html);
                                    
                                    
    
                                    //cur_dialog_temp.fields.concat(fields_profile);
                    
                            
                                    
                                }else{
                                    frappe.msgprint(`No existen perfiles disponibles para el requerimiento seleccionado`,'Asignar requerimiento');
                                }               
                    
                            
                                //frm.reload_doc();
                            }).fail(() => {
                                frappe.msgprint(__(""));
                            });
                        }else{
                            cur_dialog_temp.fields_dict.html_perfil.$wrapper.html("");
                        }
                                                
                        

                    



                } },
				{	"fieldtype": "Select", "label": __("Tipo de asignación"),
					"fieldname": "tipo_asignacion",
					"options": ["Especialistas seleccionados","Todos los especialistas filtrados"],
					"reqd": 1,
					"default": "Especialistas seleccionados" },
                {
                    fieldtype: "HTML",
                    fieldname: "help",
                    options: `<label class="control-label">
                        ${__("Selecciona el perfil que va a ser asignado")}
                    </label> <br> <br>`,
                },
                {	"fieldtype": "HTML", 
                "label": __("html_perfil"),
                "fieldname": "html_perfil",
                "options": ""
                 }
                    
                
			],
			primary_action_label: __("Agregar"),
			primary_action: (args) => {

                
                let selected_attributes = [];
                cur_dialog_temp.$wrapper.find('.form-group').each((i, col) => {
                    //if(i===0) return;
                    let attribute_name = $(col).data("fieldname");
                    let attribute_value = $(col).data("fieldvalue");
                    //selected_attributes[attribute_name] = [];
                    let checked_opts = $(col).find('.input');
                    checked_opts.each((i, opt) => {
                        if($(opt).is(':checked')) {
                            //selected_attributes[attribute_name].push($(opt).data('fieldname'));
                            selected_attributes.push({'name':attribute_name, 'especialidad':attribute_value});
                        }
                    });
                });


                console.log("encontrados");
                
                console.log(selected_attributes);

                if (selected_attributes.length >   1 ){
                    frappe.msgprint(__("Solo puede seleccionar 1 perfil"));
                    return;
                }

                if (selected_attributes.length ==  0 ){
                    frappe.msgprint(__("Seleccione el perfil que va a ser asignado a los especialistas asignado(s)"));
                    return;
                }

                if (args.tipo_asignacion == "Especialistas seleccionados" && cur_list.get_checked_items(true).length == 0 ){
                    frappe.msgprint(__("Seleccione uno o varios especialistas para poder asignarlo(s) al requerimiento."));
                    return;

                }
                if (args.tipo_asignacion == "Todos los especialistas filtrados" &&  cur_list.data.length == 0) {
                    frappe.msgprint(__("No se ha encontrado especialistas para agregar."));
                    return;
                }

                let tipo_asignacion =  args.tipo_asignacion == "Especialistas seleccionados"? "los especialistas seleccionados": "todos los especialistas filtrados"


				frappe.confirm(
					__("¿Desea agregar " + tipo_asignacion + " al requerimiento "+ args.requerimiento +"? "),
					() => {
                        
                        var filters  = cur_list.get_filters_for_args();

                        frappe.call({
                            type:"GET",
                            args: {'filters': filters, checked_items: Array.from(cur_list.get_checked_items(true)), type:args.tipo_asignacion,requerimiento:args.requerimiento,lista_perfil:selected_attributes },
                            method:"hubteam.hubteam.doctype.requerimiento.requerimiento.add_team",
                            freeze: true,
                        }).done((data) => {

                            if(data.message>= 1){
                                if (cur_dialog_temp.hide != undefined){
                                    cur_dialog_temp.hide();
                                }
                                
                                frappe.msgprint(` <a class="btn btn-primary btn-sm primary-action" onclick="frappe.set_route('Form', 'Requerimiento', '`+ args.requerimiento+`')" style="margin-right: 5px;">`+ 'Ver requerimiento' + `</a>`,'Asignación completada para el requerimiento ' + args.requerimiento);
                            }                         

                            
                            //console.log(data);
                            
                            //window.open(data.message.replace("get_url","exportar"));

                            //frm.reload_doc();
                        }).fail(() => {
                            frappe.msgprint(__(""));
                        });
                    },
                    //false
                    () => {

                    }
				);

				

			}
		});

		cur_dialog_temp.show();
		cur_dialog_temp.$wrapper.find('.modal-dialog').css("width", "400px");
	}

} 